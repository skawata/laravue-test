<?php

use Illuminate\Http\Request;
use App\Image;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tasks', 'TasksController@index');
Route::post('/task/create', 'TasksController@create');
Route::post('/task/{task}/delete', 'TasksController@delete');

Route::get('/spa/', function () {
    return view('spa.index');
});

function getImages()
{
    return collect(Image::all())->map(function ($image) {
        return [
            'src' => '/image/' . $image->filename,
            'id' => $image->id
        ];
    });
}
Route::get('/api/images', function () {
    return response()->json([
        'images' => getImages()
    ]);
});

Route::delete('/api/image/{image}', function (Image $image) {
    $image->delete();
    return response()->json(['images' => getImages()]);
});

Route::post('/api/image/create', function (Request $request) {
    $image = $request->file('image');
    $filename = bin2hex(random_bytes(32)) . '.' . $image->getClientOriginalExtension();
    $image->move(public_path() . '/image/', $filename);
    Image::create([
        'filename' => $filename,
        'comment'  => 'hoge'
    ]);
    return response()->json(['images' => getImages()]);
});
