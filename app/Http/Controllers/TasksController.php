<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('task.index', ['tasks' => $tasks]);
    }

    public function create(Request $request)
    {
        Task::create([
            'text' => $request['text'],
            'status' => 0
        ]);
        return redirect('/tasks/');
    }

    public function delete(Task $task)
    {
        $task->delete();
        return redirect('/tasks/');
    }
}
