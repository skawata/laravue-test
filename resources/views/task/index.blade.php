<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        @foreach ($tasks as $task)
        <div class="alert alert-info" role="alert">
            {{$task->text}}
            <form action="/task/{{$task->id}}/delete" method="post">
                {{csrf_field()}}
                <button class="btn btn-danger">
                    DELETE
                </button>
            </form>
        </div>
        @endforeach
    </div>
    <form action="/task/create" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <input class="form-control" name="text" placeholder="text">
            <button class="btn btn-primary">ADD</button>
        </div>
    </form>
</body>
</html>